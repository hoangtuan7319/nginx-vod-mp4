FROM nginx:1.25.2 AS builder

ENV NGINX_VOD_MODULE_VERSION=1.32

RUN export DEBIAN_FRONTEND=noninteractive && apt update \
    && apt-get install -y --no-install-recommends \
    wget \
    curl \
    gcc \
    make \
    libc6-dev \
    libssl-dev \
    libpcre2-dev \
    zlib1g-dev \
    linux-headers-amd64 \
    gnupg \
    libxslt-dev \
    libgd-dev \
    libgeoip-dev

RUN wget "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O nginx.tar.gz && \
    wget https://github.com/kaltura/nginx-vod-module/archive/refs/tags/${NGINX_VOD_MODULE_VERSION}.tar.gz -O nginx-vod-module.tar.gz

RUN CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
    && tar -zxf nginx.tar.gz \
    && tar -xzf nginx-vod-module.tar.gz \
    && cd nginx-$NGINX_VERSION \
    && sh -c "./configure --with-compat $CONFARGS --with-file-aio --with-threads --with-cc-opt='-O3 -mpopcnt' --add-dynamic-module=/nginx-vod-module-${NGINX_VOD_MODULE_VERSION}" \
    && make && make install


FROM nginx:1.25.2

COPY --from=builder /usr/sbin/nginx /usr/sbin/nginx
COPY --from=builder /usr/lib/nginx /usr/lib/nginx
COPY --from=builder /etc/nginx /etc/nginx

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/conf.d /etc/nginx/conf.d
COPY www /www

EXPOSE 80
EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]